plugins {
    kotlin("jvm")
}

kotlin {
    sourceSets {
        val kotlinLoggingJvmVersion: String by project
        val logbackVersion: String by project
        val kotlinVersion: String by project
        val kspVersion: String by project
        val mockkVersion: String by project

        @Suppress("UNUSED_VARIABLE")
        val main by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")

                implementation("com.google.devtools.ksp:symbol-processing-api:$kspVersion")
                implementation("com.squareup:javapoet:1.12.1")
                implementation("com.squareup:kotlinpoet:1.13.0")
                implementation("com.squareup:kotlinpoet-ksp:1.13.0")
            }
            @Suppress("UNUSED_VARIABLE")
            val test by getting {
                dependencies {
                    implementation(kotlin("test-junit"))
                    implementation("io.mockk:mockk:$mockkVersion")

                    implementation(project(":starwars"))
                }
            }
        }
        dependencies {
            // log
            implementation("ch.qos.logback:logback-classic:$logbackVersion")
            implementation("io.github.microutils:kotlin-logging-jvm:$kotlinLoggingJvmVersion")

            implementation(project(":common"))
            implementation(project(":ioc"))
        }
    }
    jvmToolchain(11)
}

sourceSets.main {
    java.srcDirs("src/main/kotlin")
}

