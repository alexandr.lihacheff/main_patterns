package ru.drvshare.main_patterns.codegen

import com.google.devtools.ksp.getDeclaredFunctions
import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSTypeReference
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ksp.toClassName
import com.squareup.kotlinpoet.ksp.toKModifier
import com.squareup.kotlinpoet.ksp.toTypeName
import ru.drvshare.main_patterns.common.UObject
import ru.drvshare.main_patterns.ioc.IoC

class AdapterAnnotationProcessor(
    private val codeGenerator: CodeGenerator,
    private val logger: KSPLogger
) : SymbolProcessor {
    companion object {
    }

    private val processed = mutableListOf<ClassName>()

    /** Избегаем двойной обработки аннотаций */
    private fun checkProcessed(classSpec: ClassName): Boolean {
        return when {
            processed.contains(classSpec) -> true
            else -> {
                processed.add(classSpec)
                false
            }
        }
    }

    override fun process(resolver: Resolver): List<KSAnnotated> {
        val declarations = resolver.getSymbolsWithAnnotation(AdapterAnnotation::class.qualifiedName!!, inDepth = false)
            .filterIsInstance<KSClassDeclaration>()

        declarations.forEach { declaration ->
            val classSpec = declaration.asType(listOf()).toClassName()
            if (checkProcessed(classSpec)) return@forEach

            val annotatedClassName = createClassName(classSpec)

            /** извлекаем типы и названия полей исходного класса */
            val properties = declaration.getAllProperties()

            /** извлекаем методы исходного класса */
            val functions = declaration.getDeclaredFunctions()

            /** список свойств */
            val poetProperties = mutableListOf<PropertySpec>()

            /** список полей конструктора */
            val constructorParams = mutableListOf<ParameterSpec>()
            val resultTemplate = mutableListOf<String>()

            /** добавляем поле в коструктор */
            poetProperties.add(
                PropertySpec.builder("obj", UObject::class)
                    .addModifiers(KModifier.PRIVATE)
                    .initializer("obj").build()
            )
            constructorParams.add(ParameterSpec("obj", UObject::class.asTypeName()))
            resultTemplate.add("obj=\$obj")

            /** создаём список свойств */
            properties.forEach {
                val name = it.simpleName.getShortName()
                logger.info("param name = $name")
                poetProperties.add(
                    PropertySpec.builder(name, it.type.resolve().toClassName())
                        .mutable(true)
                        .addModifiers(KModifier.OVERRIDE)
                        .getter(
                            FunSpec.getterBuilder()
                                .addCode("return IoC.resolve(\"Operations.IMovable:$name.get\", obj)")
                                .build()
                        )
                        .setter(
                            FunSpec.setterBuilder()
                                .addParameter("value", it.type.resolve().toClassName())
                                .addCode("return IoC.resolve(\"Operations.IMovable:$name.set\", obj, value)")
                                .build()
                        )
                        .build()
                )
                resultTemplate.add("$name=\$$name")
            }

            val funSpecs = functions.map {
                val name = it.simpleName.getShortName()
                logger.info("fun name = $name")
                /** теперь генерируем функцию и наполняем ее кодом */
                val code = CodeBlock.builder().addStatement(DEFAULT_METHOD).build()
                val returnType: KSTypeReference = it.returnType!!

                val kModifierList: List<KModifier> = it.modifiers.map { modifier -> modifier.toKModifier()!! }
                FunSpec.builder(name)
                    .returns(returnType.toTypeName())
                    .addModifiers(KModifier.OVERRIDE)
                    .addModifiers(kModifierList).addCode(code)
                    .build()

            }.toList()

            val generatedClass =
                TypeSpec.classBuilder(annotatedClassName)
                    .addProperties(poetProperties)
                    .addSuperinterface(classSpec.topLevelClassName())
                    .primaryConstructor(
                        FunSpec.constructorBuilder().addParameters(constructorParams).build()
                    )
                    .addFunctions(funSpecs)
                    .build()

            //генерируем исходный код
            val file = FileSpec.builder(classSpec.packageName, "$annotatedClassName.kt")
                .addImport("ru.drvshare.main_patterns.ioc", IoC::class.simpleName!!)
                .addType(
                    generatedClass
                ).build()
            //и записываем его в build/generated/ksp/main/kotlin/<package>/Annotated$className.kt
            val codeFile = try {
                logger.info(annotatedClassName)
                codeGenerator.createNewFile(
                    dependencies = Dependencies(false, declaration.containingFile!!),
                    packageName = classSpec.packageName,
                    fileName = annotatedClassName,
                    extensionName = "kt"
                )
            } catch (e: FileAlreadyExistsException) {
                println(e)
                throw Exception(e)
            }
            val writer = codeFile.bufferedWriter()
//                writer.append("//Generated file")
            file.writeTo(writer)
            //не забываем сохранить буфер в файл
            writer.flush()
            writer.close()
        }
        return declarations.toList()
    }


    override fun finish() {
        logger.info("Annotation processor is finished")
    }

    override fun onError() {
        logger.info("Annotation processor is Error")
    }

}

class AdapterAnnotationProvider : SymbolProcessorProvider {
    override fun create(environment: SymbolProcessorEnvironment): SymbolProcessor =
        AdapterAnnotationProcessor(environment.codeGenerator, environment.logger)
}
