package ru.drvshare.main_patterns.codegen

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.asClassName
import kotlin.reflect.KClass

const val DEFAULT_METHOD = "TODO(\"Not yet implemented\")"

fun createClassName(classSpec: ClassName) = "Adapter${classSpec.simpleName.substring(1)}"

fun createFoolClassName(classSpec: ClassName) = "${classSpec.packageName}.${createClassName(classSpec)}"

fun <T : Any> createFoolClassName(clazz: KClass<T>) = createFoolClassName(clazz.asClassName())
