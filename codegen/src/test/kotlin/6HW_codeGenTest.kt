package ru.drvshare.main_patterns.codegen

import com.squareup.kotlinpoet.asClassName
import io.mockk.InternalPlatformDsl.toStr
import io.mockk.mockk
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.UObject
import ru.drvshare.main_patterns.ioc.IoC
import ru.drvshare.main_patterns.ioc.command.InitScopesCommand
import ru.drvshare.main_patterns.starwars.AdapterGeneratorStrategy
import ru.drvshare.main_patterns.starwars.cmds.move.AdapterMovable
import ru.drvshare.main_patterns.starwars.cmds.move.IMovable
import ru.drvshare.main_patterns.starwars.cmds.move.Vector
import kotlin.reflect.KClass
import kotlin.test.*


class `6HW_codeGenTest` {
    @BeforeTest
    fun init() {
        InitScopesCommand().execute()

        IoC.resolve<ICommand>(
            "IoC.Register",
            "Adapter.Get",
            { it: Array<out Any> -> AdapterGeneratorStrategy(it[0] as KClass<*>, it[1] as UObject) }
        ).execute()
    }


    @Test
    fun `check getting`() {
        val uo = mockk<UObject>()
        val ss = Class.forName(createFoolClassName(IMovable::class.asClassName())).getDeclaredConstructor(UObject::class.java).newInstance(uo) as IMovable
        println(ss is AdapterMovable)
    }

    @Test
    fun `getting an adapter - integration test`() {

        val uo = mockk<UObject>()
        val adapter = IoC.resolve<AdapterGeneratorStrategy<IMovable>>("Adapter.Get", IMovable::class, uo)()

        assertEquals("AdapterMovable", adapter::class.simpleName)
    }

    @Test
    fun `check an adapter - integration test`() {

        IoC.resolve<ICommand>(
            "IoC.Register",
            "Operations.IMovable:position.get",
            { it: Array<out Any> -> Vector(1,1) }
        ).execute()

        val uo = mockk<UObject>()
        val adapter = IoC.resolve<AdapterGeneratorStrategy<IMovable>>("Adapter.Get", IMovable::class, uo)()

        assertEquals(Vector(1, 1), adapter.position)
    }

    @Test
    fun `checked an finish - integration test`() {

        val uo = mockk<UObject>()
        val adapter = IoC.resolve<AdapterGeneratorStrategy<IMovable>>("Adapter.Get", IMovable::class, uo)()

        println(assertFailsWith<NotImplementedError> {
            adapter.finish()
        }.toStr())
    }


}
