
plugins {
    kotlin("jvm") apply false
}

buildscript {
    val kotlinVersion: String by project
    dependencies {
        classpath(kotlin("gradle-plugin", version = kotlinVersion))
    }
}

group = "ru.drvshare.main_patterns"
version = "0.0.1-SNAPSHOT"

allprojects {
    apply(plugin = "kotlin")

    repositories {
        google()
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "11"
        }
    }
}

subprojects {
    group = rootProject.group
    version = rootProject.version
}
