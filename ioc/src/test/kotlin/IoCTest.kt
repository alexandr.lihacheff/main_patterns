package ru.drvshare.main_patterns.ioc

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.IScope
import ru.drvshare.main_patterns.ioc.command.InitScopesCommand
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class IoCTest {
    @BeforeTest
    fun init() {
        InitScopesCommand().execute()
    }

    @Test
    fun `Scopes Root`() {
        val resolve = IoC.resolve<IScope>("Scopes.Root")
        assertNotNull(resolve)
        assertEquals(5, resolve.dependencies.size)
    }

    @Test
    fun `Scopes New`() {
        val resolve = IoC.resolve<IScope>("Scopes.New", IoC.resolve<IScope>("Scopes.Root"))
        assertNotNull(resolve)
    }

    @Test
    fun `IoC Register`() {
        val args: IScope = IoC.resolve<IScope>("Scopes.New", IoC.resolve<IScope>("Scopes.Root"))
        IoC.resolve<ICommand>(
            "Scopes.Current.Set",
            args
        ).execute()
        IoC.resolve<ICommand>(
            "IoC.Register",
            "Test.Dependency",
            { it: Any -> 1 }
        ).execute()

        val resolve = IoC.resolve<Int>("Test.Dependency")

        assertEquals(1, resolve)
    }

    /** FIXME Всё таки доделаю многопоточку!!! */
/*    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun qqqq() {
        val thread1: ExecutorCoroutineDispatcher = newSingleThreadContext("Thread1")
        val thread2: ExecutorCoroutineDispatcher = newSingleThreadContext("Thread2")

        runBlocking {
            async(thread1) {
                val name = Thread.currentThread().name
                println(" ////  //// $name")
                val args: IScope = IoC.resolve<IScope>("Scopes.New", IoC.resolve<IScope>("Scopes.Root"))
                IoC.resolve<ICommand>(
                    "Scopes.Current.Set",
                    args
                ).execute()
                IoC.resolve<ICommand>(
                    "IoC.Register",
                    "Test.Dependency",
                    { it: Any -> name }
                ).execute()
                delay(10L)

                val resolve = IoC.resolve<String>("Test.Dependency")
                if (name != resolve) throw Exception()
            }
            async(thread2) {
                val name = Thread.currentThread().name
                println(" ////  //// $name")
                val args: IScope = IoC.resolve<IScope>("Scopes.New", IoC.resolve<IScope>("Scopes.Root"))
                IoC.resolve<ICommand>(
                    "Scopes.Current.Set",
                    args
                ).execute()
                IoC.resolve<ICommand>(
                    "IoC.Register",
                    "Test.Dependency",
                    { it: Any -> name }
                ).execute()
                delay(8L)

                val resolve = IoC.resolve<String>("Test.Dependency")
                if (name != resolve) throw Exception()
            }
        }
    }*/
}


