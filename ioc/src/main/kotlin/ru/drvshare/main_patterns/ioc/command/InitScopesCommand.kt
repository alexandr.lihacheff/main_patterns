package ru.drvshare.main_patterns.ioc.command

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.IScope
import ru.drvshare.main_patterns.ioc.IoC
import ru.drvshare.main_patterns.ioc.Scope
import ru.drvshare.main_patterns.ioc.ScopeBaseDependencyStrategy
import java.util.concurrent.ConcurrentHashMap

class InitScopesCommand : ICommand {
    override fun execute() {
        if (ScopeBaseDependencyStrategy.Root != null) return

        val dependencies: ConcurrentHashMap<String, ((Array<out Any>) -> Any)> =
            ConcurrentHashMap<String, ((Array<out Any>) -> Any)>()
                .also { deps ->
                    deps.putIfAbsent("Scopes.Storage") { ConcurrentHashMap<String, (Array<out Any>) -> Any>() }

                    deps.putIfAbsent("Scopes.New") {
                        Scope(dependencies = IoC.resolve("Scopes.Storage"), parent = it[0] as? IScope)
                    }

                    deps.putIfAbsent("Scopes.Current") {
                        ScopeBaseDependencyStrategy.CurrentScope.get() ?: ScopeBaseDependencyStrategy.DefaultScope
                    }

                    deps.putIfAbsent("Scopes.Current.Set") {
                        SetCurrentScopeCommand(scope = (it[0] as IScope))
                    }

                    deps.putIfAbsent("IoC.Register") {
                        @Suppress("UNCHECKED_CAST") // Я знаю лучше
                        IoCRegisterCommand(key = it[0] as String, strategy = it[1] as (Array<out Any>) -> Any)
                    }
                }

        val scope = Scope(dependencies, null)
        IoC.resolve<ICommand>("IoC.SetupStrategy", ScopeBaseDependencyStrategy::resolve).execute()

        ScopeBaseDependencyStrategy.Root = scope
        SetCurrentScopeCommand(scope).execute()
    }
}

