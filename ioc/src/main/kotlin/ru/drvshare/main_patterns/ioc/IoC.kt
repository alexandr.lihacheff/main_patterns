package ru.drvshare.main_patterns.ioc

import ru.drvshare.main_patterns.ioc.command.SetupStrategyCommand

public object IoC {
    var strategy: (String, Array<out Any>) -> Any = ::defaultStrategy
        internal set

    inline fun <reified T> resolve(key: String, vararg args: Any): T {
        return strategy(key, args) as T
    }

}

private fun defaultStrategy(key: String, args: Array<out Any>): Any {
    @Suppress("UNCHECKED_CAST")
    return when (key) {
        "IoC.SetupStrategy" -> SetupStrategyCommand(args[0] as (String, Array<out Any>) -> Any)
        "IoC.Default" -> ::defaultStrategy
        else -> throw IllegalArgumentException("Unknown IoC dependency")
    }
}
