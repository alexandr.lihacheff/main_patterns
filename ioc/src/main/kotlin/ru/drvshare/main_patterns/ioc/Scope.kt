package ru.drvshare.main_patterns.ioc

import ru.drvshare.main_patterns.common.IScope
import java.util.concurrent.ConcurrentHashMap

class Scope(
    override val dependencies: ConcurrentHashMap<String, (Array<out Any>) -> Any>,
    private val parent: IScope?,
) : IScope {
    override fun resolve(key: String, args: Array<out Any>): Any? {
        return dependencies[key]?.invoke(args) ?: parent?.resolve(key, args)
    }
}

