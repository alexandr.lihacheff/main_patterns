package ru.drvshare.main_patterns.ioc.command

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.IScope
import ru.drvshare.main_patterns.common.KLOGGER
import ru.drvshare.main_patterns.ioc.ScopeBaseDependencyStrategy

class SetCurrentScopeCommand(private val scope: IScope) : ICommand {
    override fun execute() {
        ScopeBaseDependencyStrategy.CurrentScope.set(scope)
        KLOGGER.debug("set the current scope for ${Thread.currentThread().name}")
    }
}
