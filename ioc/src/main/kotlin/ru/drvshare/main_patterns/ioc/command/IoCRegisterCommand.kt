package ru.drvshare.main_patterns.ioc.command

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.ioc.ScopeBaseDependencyStrategy

//@Throws Exception
class IoCRegisterCommand(private val key: String, private val strategy: (Array<out Any>) -> Any) : ICommand {
    override fun execute() {
        ScopeBaseDependencyStrategy.CurrentScope.get().dependencies.putIfAbsent(key, strategy)// ?: throw Exception("Не удалось зарегистрировать зависимость")
    }
}
