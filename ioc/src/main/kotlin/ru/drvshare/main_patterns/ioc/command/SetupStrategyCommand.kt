package ru.drvshare.main_patterns.ioc.command

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.ioc.IoC

class SetupStrategyCommand(private val newStrategy: (String, Array<out Any>) -> Any): ICommand {
    override fun execute() {
        IoC.strategy = newStrategy
    }
}
