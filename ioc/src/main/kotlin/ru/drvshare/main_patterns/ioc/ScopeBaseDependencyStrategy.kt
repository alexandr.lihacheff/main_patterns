package ru.drvshare.main_patterns.ioc

import ru.drvshare.main_patterns.common.IScope

object ScopeBaseDependencyStrategy {
    var Root: IScope? = null
    var DefaultScope: () -> IScope = { Root!! }
    val CurrentScope = ThreadLocal<IScope>()

    fun resolve(key: String, vararg args: Any): Any {
        return when (key) {
            "Scopes.Root" -> Root
            else -> CurrentScope.get()?.resolve(key, args) ?: DefaultScope()
        }!!
    }
}
