plugins {
    kotlin("jvm")
}

kotlin {
    sourceSets {
        val mockkVersion: String by project
        val kotlinVersion: String by project
        val kotlinLoggingJvmVersion: String by project
        val logbackVersion: String by project
        val coroutinesVersion: String by project

        @Suppress("UNUSED_VARIABLE")
        val main by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")

                implementation(project(":common"))
            }
        }

        @Suppress("UNUSED_VARIABLE")
        val test by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("io.mockk:mockk:$mockkVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
            }
        }
        dependencies {
            // log
            implementation("ch.qos.logback:logback-classic:$logbackVersion")
            implementation("io.github.microutils:kotlin-logging-jvm:$kotlinLoggingJvmVersion")

        }
    }
    jvmToolchain(11)

}
