
rootProject.name = "main_patterns"

pluginManagement {
    plugins {
        val kotlinVersion: String by settings
        val kspVersion: String by settings

        id("com.google.devtools.ksp") version kspVersion apply false
        kotlin("jvm") version kotlinVersion apply false
    }
    repositories {
        gradlePluginPortal()
        mavenCentral()
    }
}
include("ioc")
//include("quadratic")
include("starwars")
include("common")
include("queue")
include("codegen")
