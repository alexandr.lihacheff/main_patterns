package ru.drvshare.main_patterns.starwars.queue.commands

import ru.drvshare.main_patterns.common.ContextObject
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.ioc.IoC
import ru.drvshare.main_patterns.starwars.queue.IQueue

class SoftStopCommand : ICommand {
    private val contextObject = IoC.resolve<ContextObject>("ContextObject")

    override fun execute() {
        contextObject.setProperties("process", {
            contextObject.getPropertyAs<IQueue>("queue").also {
                it.poll().execute()
                it.takeIf { it.isEmpty() }?.let {
                    contextObject.setProperties("Interrupt", true)
                }
            }
        })
    }
}
