package ru.drvshare.main_patterns.starwars.queue

import ru.drvshare.main_patterns.common.ICommand
import java.util.concurrent.BlockingQueue
import java.util.concurrent.TimeUnit

open class CmdsQueueAdapter(private val queue: BlockingQueue<ICommand>) : IQueue {

    override fun put(cmd: ICommand) = queue.put(cmd)

    override fun poll(): ICommand = queue.poll(5, TimeUnit.SECONDS)!!

    override fun size(): Int = queue.size

    override fun isEmpty(): Boolean = queue.isEmpty()

    override fun toString(): String {
        return "CmdsQueueAdapter(queue=$queue)"
    }
}
