package ru.drvshare.main_patterns.starwars.queue

import ru.drvshare.main_patterns.common.ICommand


interface IQueue {
    fun put(cmd: ICommand)
    fun poll(): ICommand
    fun size(): Int
    fun isEmpty(): Boolean
}
