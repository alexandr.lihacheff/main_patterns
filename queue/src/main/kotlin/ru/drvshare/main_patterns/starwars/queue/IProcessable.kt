package ru.drvshare.main_patterns.starwars.queue

interface IProcessable {

    fun interrupt(): Boolean

    fun process();
}
