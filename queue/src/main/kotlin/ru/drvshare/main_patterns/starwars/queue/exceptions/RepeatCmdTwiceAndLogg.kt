package ru.drvshare.main_patterns.starwars.queue.exceptions

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.queue.CmdQueue
import ru.drvshare.main_patterns.starwars.queue.commands.RepeatCmd

const val MAX_REPEAT = 2

class RepeatCmdTwiceAndLogg(override val exception: Exception, override val cmd: ICommand) : IExceptionHandlerCmd {
    companion object {
        private val countCmd: ThreadLocal<MutableMap<Int, Int>> = ThreadLocal<MutableMap<Int, Int>>().also { it.set(mutableMapOf()) }
    }
    override fun execute() {
        val hashCode = cmd.hashCode()
        val count = countCmd.get()
        val i = count[hashCode]

        if (i == null) {
            count[hashCode] = 1
            CmdQueue.add(RepeatCmd(cmd))
        } else if (i < MAX_REPEAT) {
            count[hashCode] = i + 1
            CmdQueue.add(RepeatCmd(cmd))
        } else {
            count.remove(hashCode)
            CmdQueue.add(LoggingHandler(exception, cmd))
        }

//        CmdQueue.add(RepeatCmd(exception, cmd))
    }
}


