package ru.drvshare.main_patterns.starwars.queue.exceptions

import ru.drvshare.main_patterns.common.ICommand


class CommandExceptionHandler(override val exception: Exception, override val cmd: ICommand) : IExceptionHandlerCmd {
    override fun execute() {
        TODO("Not yet implemented")
    }
}
