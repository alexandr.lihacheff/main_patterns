package ru.drvshare.main_patterns.starwars.queue

import ru.drvshare.main_patterns.common.ICommand
import java.util.*

@Deprecated("Только для старых тестов")
object CmdQueue : IQueue {
    private val queue: ThreadLocal<Queue<ICommand>> = ThreadLocal<Queue<ICommand>>().also {
        it.set(LinkedList<ICommand>())
    }

    fun add(cmd: ICommand) {
        val add = queue.get().add(cmd)
        if (add) run()
    }

    override fun put(cmd: ICommand): Unit = add(cmd)

    override fun poll(): ICommand = queue.get().poll()
    override fun size(): Int {
        TODO("Not yet implemented")
    }

    override fun isEmpty(): Boolean {
        TODO("Not yet implemented")
    }

    private fun run() {
        while (queue.get().isNotEmpty()) {
            poll().execute()
        }
    }
}
