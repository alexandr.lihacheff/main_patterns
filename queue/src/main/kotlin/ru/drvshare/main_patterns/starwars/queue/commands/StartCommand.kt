package ru.drvshare.main_patterns.starwars.queue.commands

import ru.drvshare.main_patterns.common.ContextObject
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.KLOGGER
import ru.drvshare.main_patterns.ioc.IoC
import ru.drvshare.main_patterns.starwars.queue.CmdsConsumer
import ru.drvshare.main_patterns.starwars.queue.CmdsQueueAdapter
import ru.drvshare.main_patterns.starwars.queue.IQueue
import ru.drvshare.main_patterns.starwars.queue.Processable
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

class StartCommand : ICommand {
    private val contextObject = IoC.resolve<ContextObject>("ContextObject")

    override fun execute() {
        contextObject.setProperties("Interrupt", false)
        val queue: BlockingQueue<ICommand> = LinkedBlockingQueue(10)
        contextObject.setProperties("queue", CmdsQueueAdapter(queue))
        contextObject.setProperties("process", {
            val command = contextObject.getPropertyAs<IQueue>("queue").poll()
            command.execute()
        })

        KLOGGER.debug("StartCommand")
        CmdsConsumer(Processable(contextObject)).execute()

    }
}
