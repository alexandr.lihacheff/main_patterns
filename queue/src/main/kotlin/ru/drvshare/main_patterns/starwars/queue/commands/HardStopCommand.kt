package ru.drvshare.main_patterns.starwars.queue.commands

import ru.drvshare.main_patterns.common.ContextObject
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.ioc.IoC


class HardStopCommand : ICommand {
    private val contextObject = IoC.resolve<ContextObject>("ContextObject")

    override fun execute() {
        contextObject.setProperties("Interrupt", true)
    }
}
