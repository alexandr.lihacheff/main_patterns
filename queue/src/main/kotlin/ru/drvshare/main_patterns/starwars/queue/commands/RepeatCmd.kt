package ru.drvshare.main_patterns.starwars.queue.commands

import ru.drvshare.main_patterns.common.ICommand

class RepeatCmd(private val cmd: ICommand) : ICommand {
    override fun execute() {
        cmd.execute()
    }
}
