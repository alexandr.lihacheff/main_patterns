package ru.drvshare.main_patterns.starwars.queue.exceptions

import ru.drvshare.main_patterns.common.KLOGGER
import ru.drvshare.main_patterns.common.ICommand

class LoggingHandler(override val exception: Exception, override val cmd: ICommand) : IExceptionHandlerCmd {
    override fun execute() {
        KLOGGER.info("$exception, CMD = ${cmd::class.qualifiedName}")
    }
}
