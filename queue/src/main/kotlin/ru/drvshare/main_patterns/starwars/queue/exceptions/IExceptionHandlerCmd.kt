package ru.drvshare.main_patterns.starwars.queue.exceptions

import ru.drvshare.main_patterns.common.ICommand


/** Интерфейс обработчика исключения */
interface IExceptionHandlerCmd : ICommand {
    val exception: Exception
    val cmd: ICommand
}
