package ru.drvshare.main_patterns.starwars.queue.exceptions

class CommandException(message: String?) : Exception(message) {
    companion object {
        private const val serialVersionUID: Long = -354335235965250455L
    }
}
