package ru.drvshare.main_patterns.starwars.queue.exceptions

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.queue.CmdQueue
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

object ExceptionHandler {

    private val store: MutableMap<String, Map<String, KClass<out IExceptionHandlerCmd>>> = mutableMapOf()

    fun handle(e: Exception, c: ICommand): ICommand {
        val exType: String? = e::class.qualifiedName
        val cmType: String? = c::class.qualifiedName
        val clazz = store[cmType]?.get(exType)
        val cmd = clazz?.primaryConstructor?.call(e, c) ?: DefaultHandler(e, c)
        CmdQueue.add(cmd)
        return cmd
    }

    fun register(exType: KClass<out Exception>, cmd: KClass<out ICommand>, hndl: KClass<out IExceptionHandlerCmd>) {
        store[cmd.qualifiedName!!] = mapOf(exType.qualifiedName!! to hndl)
    }
}
