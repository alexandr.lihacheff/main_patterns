package ru.drvshare.main_patterns.starwars.queue

import ru.drvshare.main_patterns.common.ContextObject

class Processable(private val contextObject: ContextObject) : IProcessable {

    override fun interrupt(): Boolean {
        return contextObject.getPropertyAs<Boolean>("Interrupt")
    }

    override fun process() {
        contextObject.getPropertyAs<Function0<Unit>>("process")()
    }
}
