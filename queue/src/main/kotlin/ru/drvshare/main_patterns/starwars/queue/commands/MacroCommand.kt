package ru.drvshare.main_patterns.starwars.queue.commands

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.queue.exceptions.CommandException

class MacroCommand(private val cmdsArray: ArrayList<ICommand>) : ICommand {
    override fun execute() {
        try {

            cmdsArray.forEach {
                it.execute()
            }
        }catch (ex: Exception){
            throw CommandException("MacroCommand is stopped")
        }
    }
}
