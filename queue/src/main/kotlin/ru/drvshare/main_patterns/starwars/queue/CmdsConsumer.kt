package ru.drvshare.main_patterns.starwars.queue

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.KLOGGER

class CmdsConsumer(private val processable: IProcessable) : ICommand {
    private var thread: Thread

    init {
        this.thread = Thread(Runnable { loop() })

    }

    private fun loop() {
        while (!processable.interrupt()) {
            try {
                processable.process()
            } catch (e: Exception) {
                println("${Thread.currentThread().name} $e")
                TODO("Добавить обработчик исключений!")
            }
        }
        KLOGGER.info("Interrupt")
    }

    override fun execute() {
        thread.start()
    }
}
