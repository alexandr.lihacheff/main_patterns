import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import ru.drvshare.main_patterns.common.ContextObject
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.IScope
import ru.drvshare.main_patterns.common.KLOGGER
import ru.drvshare.main_patterns.ioc.IoC
import ru.drvshare.main_patterns.ioc.command.InitScopesCommand
import ru.drvshare.main_patterns.starwars.queue.IQueue
import ru.drvshare.main_patterns.starwars.queue.commands.HardStopCommand
import ru.drvshare.main_patterns.starwars.queue.commands.SoftStopCommand
import ru.drvshare.main_patterns.starwars.queue.commands.StartCommand
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.test.*

class `7HW_MultithreadedExecutionTest` {
    private lateinit var rootScope: IScope

    companion object {
        const val NUMBER = 11
        private val DISPATCHER = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
    }

    @BeforeTest
    fun init() {
        InitScopesCommand().execute()
        rootScope = IoC.resolve("Scopes.Current")

        val newScope = IoC.resolve<IScope>("Scopes.New", rootScope)
        IoC.resolve<ICommand>("Scopes.Current.Set", newScope).execute()
        val co = ContextObject()
        IoC.resolve<ICommand>("IoC.Register", "ContextObject", { it: Any -> co }).execute()
        IoC.resolve<IScope>("Scopes.Current")

        StartCommand().execute()
    }

    @Test
    fun `checking the command of HardStopCommand`() {
        HardStopCommand().execute()
        KLOGGER.debug("HardStopCommand")

        val contextObject = IoC.resolve<ContextObject>("ContextObject")
        assertEquals(true, contextObject.getPropertyAs<Boolean>("Interrupt"))
    }

    @Test
    fun `checking the queue stop by the HardStop command`() {
        val contextObject = IoC.resolve<ContextObject>("ContextObject")

        val cdl = CountDownLatch(1)
        var counter = -1
        val cmd = mockk<ICommand> {
            every { execute() } answers {
                val queue = contextObject.getPropertyAs<IQueue>("queue")
                KLOGGER.debug("Command#${++counter} queue = ${queue.size()}")
            }
        }
        val hardStop = mockk<ICommand> {
            every { execute() } answers {
                contextObject.setProperties("Interrupt", true)
                cdl.countDown()
                val queue = contextObject.getPropertyAs<IQueue>("queue")
                KLOGGER.debug("HardStop => ${++counter} countDown = ${cdl.count} queue = ${queue.size()}")
            }
        }

        val queue = contextObject.getPropertyAs<IQueue>("queue")

        runBlocking {
            flow {
                repeat(NUMBER) {
                    emit(
                        if (it == 8) queue.put(hardStop) else
                            queue.put(cmd)
                    )
                    emit(queue.put(cmd))
                }
            }
                .flowOn(DISPATCHER)
                .collect()
        }


        cdl.await(10, TimeUnit.MILLISECONDS)
        KLOGGER.debug("fine")

        assertTrue(!queue.isEmpty())
    }

    @Test
    fun `checking the command of SoftStopCommand`() {
        val contextObject = IoC.resolve<ContextObject>("ContextObject")

        val previousProcess = contextObject.getPropertyAs<Function0<Unit>>("process")
        SoftStopCommand().execute()
        val currentProcess = contextObject.getPropertyAs<Function0<Unit>>("process")




        assertNotEquals(previousProcess, currentProcess)
    }

    @Test
    fun `checking the queue stop by the SoftStop command`() {
        runBlocking {
            val contextObject = IoC.resolve<ContextObject>("ContextObject")

            val cdl = CountDownLatch(NUMBER * 2)
            var counter = -1
            val cmd = mockk<ICommand> {
                every { execute() } answers {
                    cdl.countDown()
                    val queue = contextObject.getPropertyAs<IQueue>("queue")
                    KLOGGER.debug("Command#${++counter}  countDown = ${cdl.count}  queue = ${queue.size()}")
                }
            }
            val softStop = mockk<ICommand> {
                every { execute() } answers {
                    contextObject.setProperties("process", {
                        contextObject.getPropertyAs<IQueue>("queue").also {
                            it.poll().execute()
                            it.takeIf { it.isEmpty() }?.let {
                                contextObject.setProperties("Interrupt", true)
                            }
                        }
                    })

                    val queue = contextObject.getPropertyAs<IQueue>("queue")
                    cdl.countDown()
                    KLOGGER.debug("SoftStop => ${++counter} countDown = ${cdl.count} queue = ${queue.size()}")
                }
            }

            val queue = contextObject.getPropertyAs<IQueue>("queue")
            flow {
                repeat(NUMBER) {
                    emit(
                        if (it == 8) queue.put(softStop) else
                            queue.put(cmd)
                    )
                    emit(queue.put(cmd))
                }
            }
                .flowOn(DISPATCHER)
                .collect()

            withContext(Dispatchers.IO) {
                cdl.await(10, TimeUnit.MILLISECONDS)
            }
            KLOGGER.debug("fine")
            /** Очередь пуста */
            assertTrue(queue.isEmpty())

            queue.put(cmd)
            withContext(Dispatchers.IO) {
                Thread.sleep(10)
            }
            /** Консьюмер не работает */
            assertEquals(1, queue.size())
        }
    }

}
