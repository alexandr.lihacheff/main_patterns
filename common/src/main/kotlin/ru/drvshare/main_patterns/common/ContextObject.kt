package ru.drvshare.main_patterns.common

import java.util.concurrent.ConcurrentHashMap


class ContextObject : UObject {
    private val properties: ConcurrentHashMap<String, Any> = ConcurrentHashMap<String, Any>()

    inline fun <reified T> getPropertyAs(key: String): T {
        return getProperty(key) as T
    }

    override fun getProperty(key: String): Any {
        return this.properties[key]!!
    }

    override fun setProperties(key: String, newValue: Any) {
        this.properties[key] = newValue
    }

    override fun toString(): String {
        return "ContextObject(properties=${this.properties})"
    }

}
