package ru.drvshare.main_patterns.common

interface UObject {
    fun getProperty(key: String): Any?
    fun setProperties(key: String, newValue: Any)
}
