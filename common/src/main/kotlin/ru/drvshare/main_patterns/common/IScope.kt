package ru.drvshare.main_patterns.common

import java.util.concurrent.ConcurrentHashMap

interface IScope {
    val dependencies: ConcurrentHashMap<String, (Array<out Any>) -> Any>

    fun resolve(key: String, args: Array<out Any>): Any?
}
