package ru.drvshare.main_patterns.common

/** Интерфейс Команды */
fun interface ICommand {
    fun execute()
}
