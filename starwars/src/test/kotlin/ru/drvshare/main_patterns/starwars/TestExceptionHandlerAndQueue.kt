package ru.drvshare.main_patterns.starwars

import io.mockk.every
import io.mockk.mockk
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.cmds.move.IMovable
import ru.drvshare.main_patterns.starwars.cmds.move.Move
import ru.drvshare.main_patterns.starwars.cmds.move.Vector
import ru.drvshare.main_patterns.starwars.queue.exceptions.ExceptionHandler
import ru.drvshare.main_patterns.starwars.queue.exceptions.LoggingHandler
import ru.drvshare.main_patterns.starwars.queue.exceptions.RepeatCmdTwiceAndLogg
import ru.drvshare.main_patterns.starwars.queue.CmdQueue
import java.io.IOException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestExceptionHandlerAndQueue {

    @Test
    fun `LoggingHandler simple test`() {

        ExceptionHandler.register(IOException::class, Move::class, LoggingHandler::class)
        try {
            throw IOException()
        } catch (e: Exception) {

            val c = Move(mockk<IMovable> {})
            val handle = ExceptionHandler.handle(e, c)
            assertTrue { handle is LoggingHandler }
        }
        /** FIXME Пока не придумал как тестить само логирование */
    }

    @Test
    fun `testing queue simple`() {
        class TestCmd(private var i: Array<Int>) : ICommand {
            override fun execute() {
                Thread.sleep(100)
                i[0] += 1
            }
        }

        val i = arrayOf(0)
        CmdQueue.add(TestCmd(i))
        CmdQueue.add(TestCmd(i))
        CmdQueue.add(TestCmd(i))
        assertEquals(3, i[0])
    }

    @Test
    fun `add LoggingHandler in the queue`() {
        ExceptionHandler.register(IOException::class, Move::class, LoggingHandler::class)

        val mm = mockk<MovableMock> {
            every { position } returns Vector(x = 12, y = 5)
            every { velocity } throws IOException("Какая-то ошибка при чтении скорости")
        }

        CmdQueue.add(Move(mm))
    }

    @Test
    fun `repeat the command twice and logging once`() {

        ExceptionHandler.register(IOException::class, Move::class, RepeatCmdTwiceAndLogg::class)

        val movableMock = mockk<MovableMock> {
            every { position } returns Vector(x = 12, y = 5)
            every { velocity } throws IOException("Какая-то ошибка при чтении скорости")
        }

        var cnt = 0
        val cmdMove = Move(movableMock)
        every { cmdMove.execute() } answers {
            ++cnt
            ExceptionHandler.handle(IOException(), cmdMove)
        }

        CmdQueue.add(cmdMove)

        /** cmd x 2 + logging = 3 times */
        assertEquals(3, cnt)
    }


}

