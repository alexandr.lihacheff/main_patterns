package ru.drvshare.main_patterns.starwars

import io.mockk.InternalPlatformDsl.toStr
import io.mockk.every
import io.mockk.mockk
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.cmds.move.IMovable
import ru.drvshare.main_patterns.starwars.cmds.move.Vector
import ru.drvshare.main_patterns.starwars.cmds.rotate.Direction
import ru.drvshare.main_patterns.starwars.cmds.rotate.IRotable
import java.io.IOException
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

class TestMoveAndRotate {

    @Test
    fun `positive moving test`() {
        val movable = MovableMock(Vector(x = 12, y = 5), Vector(x = -7, y = 3))
        TestMove(movable).execute()
        assertEquals(Vector(x = 5, y = 8), movable.position)
    }

    @Test
    fun `exception when reading the position of the object - moving test`() {
        val mm = mockk<IMovable> {
            every { position } throws IOException("Какая-то ошибка при чтении позиции")
            every { velocity } returns Vector(x = 12, y = 5)
        }

        println(assertFails {
            TestMove(mm).execute()
        }.toStr())
    }

    @Test
    fun `exception when reading the velocity of the object - moving test`() {
        val mm = mockk<MovableMock> {
            every { position } returns Vector(x = 12, y = 5)
            every { velocity } throws IOException("Какая-то ошибка при чтении скорости")
        }

        println(assertFails {
            TestMove(mm).execute()
        }.toStr())
    }

    @Test
    fun `exception when writing the position of the object - moving test`() {
        val mm = mockk<IMovable> {
            every { position = any() } throws InputMismatchException("Какая-то ошибка записи новой позиции")
            every { position } returns Vector(x = 12, y = 5)
            every { velocity } returns Vector(x = 12, y = 5)
        }

        println(assertFails {
            TestMove(mm).execute()
        }.toStr())
    }

    @Test
    fun `positive rotating test`() {
        val rotable = RotableMock(Direction(2, 8), 2)
        TestRotate(rotable).execute()
        assertEquals(Direction(4, 8), rotable.direction)
//        assertEquals(Vector(x = 5, y = 8), movable.position)
    }

    @Test
    fun `exception when reading the direction of the object - rotating test`() {
        val mr = mockk<IRotable> {
            every { direction } throws IOException("Какая-то ошибка при чтении направления")
            every { angularVelocity } returns 2
        }

        println(assertFails {
            TestRotate(mr).execute()
        }.toStr())
    }

    @Test
    fun `exception when reading angularVelocity of the object - rotating test`() {
        val mr = mockk<IRotable> {
            every { direction } returns Direction(2, 8)
            every { angularVelocity } throws IOException("Какая-то ошибка при чтении угловой скорости")
        }

        println(assertFails {
            TestRotate(mr).execute()
        }.toStr())
    }

    @Test
    fun `exception when writing the direction of the object - rotating test`() {
        val mr = mockk<IRotable> {
            every { direction = any() } throws InputMismatchException("Какая-то ошибка записи нового направления")
            every { direction } returns Direction(2, 8)
            every { angularVelocity } returns 2
        }

        println(assertFails {
            TestRotate(mr).execute()
        }.toStr())
    }
}

class MovableMock(override var position: Vector, override var velocity: Vector) : IMovable {
    override fun finish() {
        TODO("Not yet implemented")
    }
}

class RotableMock(override var direction: Direction, override val angularVelocity: Int) : IRotable

class TestMove(private val m: IMovable) : ICommand {
    override fun execute() {
        try {
            m.position = m.position + m.velocity
        } catch (e: Exception) {
            throw Exception("Ошибка перемещения по прямой", e)
        }
    }
}

class TestRotate(private val r: IRotable): ICommand {
    override fun execute() = try {
        r.direction = r.direction + r.angularVelocity
    } catch (e: Exception) {
        println("   $e")
        throw Exception("Ошибка Вращения", e)
    }
}
