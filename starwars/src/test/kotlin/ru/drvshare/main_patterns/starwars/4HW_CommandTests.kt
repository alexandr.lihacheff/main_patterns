package ru.drvshare.main_patterns.starwars

import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.cmds.fuel.BurnFuelCommand
import ru.drvshare.main_patterns.starwars.cmds.fuel.CheckFuelCommand
import ru.drvshare.main_patterns.starwars.cmds.fuel.IFuelable
import ru.drvshare.main_patterns.starwars.cmds.move.Vector
import ru.drvshare.main_patterns.starwars.cmds.rotate.ChangeVelocityCommand
import ru.drvshare.main_patterns.starwars.cmds.rotate.Direction
import ru.drvshare.main_patterns.starwars.cmds.rotate.Rotate
import ru.drvshare.main_patterns.starwars.queue.commands.MacroCommand
import ru.drvshare.main_patterns.starwars.queue.exceptions.CommandException
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.asserter

class `4HW_CommandTests` {
    @Test
    fun `CheckFuelCommand test - exception`() {
        val fuelable = mockk<IFuelable> {
            every { criticalFuelAmount } returns 3
            every { fuelDecrement } returns 3
            every { currentFuelAmount } returns 2
        }

        assertFailsWith<CommandException> {
            CheckFuelCommand(fuelable).execute()
        }
    }

    @Test
    fun `CheckFuelCommand test - success`() {
        val fuelable = mockk<IFuelable> {
            every { criticalFuelAmount } returns 3
            every { fuelDecrement } returns 3
            every { currentFuelAmount } returns 6
        }

        try {
            CheckFuelCommand(fuelable).execute()
        } catch (e: Exception) {
            asserter.fail("Исключения не должно быть")
//            return
        }
        asserter.assertTrue("Исключения нет!", true)
    }

    @Test
    fun `BurnFuelCommand test`() {
        val fuelable = MockFuelable(criticalFuelAmount = 3, fuelDecrement = 3, currentFuelAmount = 6)

        BurnFuelCommand(fuelable).execute()

        assertEquals(3, fuelable.currentFuelAmount)
    }

    @Test
    fun `MacroCommand simple test - Exception`() {
        val fuelable = mockk<IFuelable> {
            every { criticalFuelAmount } returns 3
            every { fuelDecrement } returns 3
            every { currentFuelAmount } returns 3
        }

        val cmds = arrayListOf<ICommand>(
            CheckFuelCommand(fuelable),
            mockk<ICommand> {
                every { execute() } just runs
            },
            BurnFuelCommand(fuelable)
        )

        assertFailsWith<CommandException> {
            MacroCommand(cmds).execute()
        }
    }

    @Test
    fun `MacroCommand with moving test - Exception`() {
        val fuelable = MockFuelable(criticalFuelAmount = 3, fuelDecrement = 3, currentFuelAmount = 6)
        val movable = MovableMock(Vector(x = 12, y = 5), Vector(x = -7, y = 3))

        val cmds = arrayListOf<ICommand>(
            CheckFuelCommand(fuelable),
            TestMove(movable),
            BurnFuelCommand(fuelable)
        )

        /** Делаем один шаг */
        MacroCommand(cmds).execute()

        assertEquals(3, fuelable.currentFuelAmount)

        assertFailsWith<CommandException> {
            /** Пытаемся сделать второй шаг и валимся с критическим объёмом */
            MacroCommand(cmds).execute()
        }

    }

    @Test
    fun `ChangeVelocityCommand test`() {
        val movable = MovableMock(Vector(x = 12, y = 5), Vector(x = 12, y = 5))

        ChangeVelocityCommand(movable, Vector(x = -7, y = 3)).execute()

        assertEquals(Vector(x = -7, y = 3), movable.velocity)
    }

    @Test
    fun `MacroCommand with the ability to rotate and change the velocity`() {
        val movable = MovableMock(Vector(x = 12, y = 5), Vector(x = 12, y = 5))
        val rotable = RotableMock(Direction(2, 8), 2)

        val cmds = arrayListOf<ICommand>(
            Rotate(rotable),

            ChangeVelocityCommand(
                movable, Vector(
                    (movable.velocity.x * cos(rotable.direction.direction / 360.0 * rotable.direction.directionsNumber)).roundToInt(),
                    (movable.velocity.y * sin(rotable.direction.direction / 360.0 * rotable.direction.directionsNumber)).roundToInt()
                )
            )
        )

        MacroCommand(cmds).execute()

        assertEquals(Vector(x = 12, y = 0), movable.velocity)
    }
}

class MockFuelable(override val criticalFuelAmount: Int, override val fuelDecrement: Int, override var currentFuelAmount: Int) : IFuelable {
}
