package ru.drvshare.main_patterns.starwars.cmds.move

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.queue.exceptions.ExceptionHandler

// Эксперимент с регистрацией
//val a = ExceptionHandler.register(IOException::class, Move::class, LoggingHandler::class)

class Move(private val m: IMovable) : ICommand {
    override fun execute() {
        try {
            m.position = m.position + m.velocity
        } catch (e: Exception) {
            ExceptionHandler.handle(e, this)
        }
    }
}
