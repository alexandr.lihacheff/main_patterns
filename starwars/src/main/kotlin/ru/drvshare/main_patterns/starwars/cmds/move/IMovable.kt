package ru.drvshare.main_patterns.starwars.cmds.move

import ru.drvshare.main_patterns.codegen.AdapterAnnotation

@AdapterAnnotation
interface IMovable {
    var position: Vector
    var velocity: Vector
    fun finish()
}
