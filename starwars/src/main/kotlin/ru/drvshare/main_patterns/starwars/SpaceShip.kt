package ru.drvshare.main_patterns.starwars

import ru.drvshare.main_patterns.common.UObject

class SpaceShip : UObject {

    private val properties: MutableMap<String, Any> = mutableMapOf()

    override fun getProperty(key: String): Any? {
        return properties[key]
    }

    override fun setProperties(key: String, newValue: Any) {
        properties[key] = newValue
    }

    override fun toString(): String {
        return "SpaceShip(properties=$properties)"
    }


}
