package ru.drvshare.main_patterns.starwars.cmds.rotate

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.cmds.move.IMovable
import ru.drvshare.main_patterns.starwars.cmds.move.Vector

class ChangeVelocityCommand(private val m: IMovable, private val newVelocity: Vector): ICommand {
    override fun execute() {
        m.velocity = newVelocity
    }
}
