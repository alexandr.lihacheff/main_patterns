package ru.drvshare.main_patterns.starwars

import ru.drvshare.main_patterns.common.UObject
import ru.drvshare.main_patterns.codegen.createFoolClassName
import kotlin.reflect.KClass

class AdapterGeneratorStrategy<T : Any>(private val type: KClass<T>, private val uo: UObject) {
    @Suppress("UNCHECKED_CAST")
    operator fun invoke(): T {
        return Class.forName(createFoolClassName(type)).getDeclaredConstructor(UObject::class.java).newInstance(uo) as T
    }
}
