package ru.drvshare.main_patterns.starwars.cmds.fuel

import ru.drvshare.main_patterns.common.UObject

class FuelableAdapter(private val o: UObject): IFuelable {
    override val criticalFuelAmount: Int
        get() = o.getProperty("criticalFuelAmount") as Int
    override val fuelDecrement: Int
        get() = o.getProperty("criticalFuelAmount") as Int
    override var currentFuelAmount: Int
        get() = o.getProperty("currentFuelAmount") as Int
        set(value) = o.setProperties("currentFuelAmount", value)
}
