package ru.drvshare.main_patterns.starwars.cmds.fuel

import ru.drvshare.main_patterns.common.ICommand

class BurnFuelCommand(private val fuel: IFuelable) : ICommand {
    override fun execute() {
        fuel.currentFuelAmount -= fuel.fuelDecrement
    }
}
