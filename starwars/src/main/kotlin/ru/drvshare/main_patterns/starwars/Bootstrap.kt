package ru.drvshare.main_patterns.starwars

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.common.UObject
import ru.drvshare.main_patterns.ioc.IoC
import ru.drvshare.main_patterns.ioc.command.InitScopesCommand
import ru.drvshare.main_patterns.starwars.cmds.move.Vector
import ru.drvshare.main_patterns.starwars.cmds.rotate.Direction
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin
import kotlin.reflect.KClass

class Bootstrap {
    fun init() {
        InitScopesCommand().execute()

        IoC.resolve<ICommand>(
            "IoC.Register",
            "Adapter.Get",
            { it: Array<out Any> -> AdapterGeneratorStrategy(it[0] as KClass<*>, it[1] as UObject) }
        ).execute()

    }

    fun initMove() {
        IoC.resolve<ICommand>(
            "IoC.Register",
            "Operations.IMovable:position.get",
            { it: Array<out Any> -> (it[0] as UObject).getProperty("Position") as Vector }
        ).execute()

        IoC.resolve<ICommand>(
            "IoC.Register",
            "Operations.IMovable:position.set",
            { it: Array<out Any> -> (it[0] as UObject).setProperties("Position", it[1] as Vector) }
        ).execute()

        IoC.resolve<ICommand>(
            "IoC.Register",
            "Operations.IMovable:velocity.get",
            { it: Array<out Any> ->
                {
                    val o = it[0] as UObject
                    val direction = o.getProperty("Direction") as Direction
                    val d = direction.direction
                    val n = direction.directionsNumber
                    val v = o.getProperty("Velocity") as Int

                    Vector(
                        (v * cos(d / 360.0 * n)).roundToInt(),
                        (v * sin(d / 360.0 * n)).roundToInt()
                    )
                }
            }
        ).execute()

        IoC.resolve<ICommand>(
            "IoC.Register",
            "Operations.IMovable:velocity.set",
            { it: Array<out Any> -> (it[0] as UObject).setProperties("Velocity", it[1] as Vector) }
        ).execute()

    }
}
