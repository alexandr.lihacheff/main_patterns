package ru.drvshare.main_patterns.starwars.cmds.rotate

import ru.drvshare.main_patterns.common.UObject

class RotableAdapter(private val o: UObject) : IRotable {
    override var direction: Direction
        get() = o.getProperty("Direction") as Direction
        set(value) = o.setProperties("Direction", value)

    override val angularVelocity get() = o.getProperty("AngularVelocity") as Int

}
