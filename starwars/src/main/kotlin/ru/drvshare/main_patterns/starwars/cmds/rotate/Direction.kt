package ru.drvshare.main_patterns.starwars.cmds.rotate

class Direction(val direction: Int, val directionsNumber: Int) {
    operator fun plus(angularVelocity: Int): Direction {
        return Direction(
            (direction + angularVelocity) % directionsNumber,
            directionsNumber
        )
    }

    override fun toString(): String {
        return "Direction(direction=$direction, directionsNumber=$directionsNumber)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Direction

        if (direction != other.direction) return false
        if (directionsNumber != other.directionsNumber) return false

        return true
    }

    override fun hashCode(): Int {
        var result = direction
        result = 31 * result + directionsNumber
        return result
    }

}
