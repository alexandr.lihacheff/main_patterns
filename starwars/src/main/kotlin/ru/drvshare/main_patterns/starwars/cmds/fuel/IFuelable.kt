package ru.drvshare.main_patterns.starwars.cmds.fuel

interface IFuelable {
    val criticalFuelAmount: Int
    val fuelDecrement: Int
    var currentFuelAmount: Int
}
