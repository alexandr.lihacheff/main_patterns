package ru.drvshare.main_patterns.starwars.cmds.move

class Vector(val x: Int, val y: Int) {
    operator fun plus(velocity: Vector): Vector {
        return Vector(
            x + velocity.x,
            y + velocity.y
        )
    }

    override fun toString(): String {
        return "Vector(x=$x, y=$y)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vector

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }
}
