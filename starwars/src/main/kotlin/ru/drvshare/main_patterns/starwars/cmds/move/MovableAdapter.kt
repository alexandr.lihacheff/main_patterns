package ru.drvshare.main_patterns.starwars.cmds.move

import ru.drvshare.main_patterns.common.UObject
import ru.drvshare.main_patterns.starwars.cmds.rotate.Direction
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin

class MovableAdapter(private val o: UObject) : IMovable {
    override var position: Vector
        get() = o.getProperty("Position") as Vector
        set(value) = o.setProperties("Position", value)

    override var velocity: Vector
        get() {
            val direction = o.getProperty("Direction") as Direction
            val d = direction.direction
            val n = direction.directionsNumber
            val v = o.getProperty("Velocity") as Int

            return Vector(
                (v * cos(d / 360.0 * n)).roundToInt(),
                (v * sin(d / 360.0 * n)).roundToInt()
            )
        }
        set(value) {
            o.setProperties("Velocity", value)
        }

    override fun finish() {
        TODO("Not yet implemented")
    }
}
