package ru.drvshare.main_patterns.starwars.cmds.rotate

import ru.drvshare.main_patterns.common.ICommand

class Rotate(private val r: IRotable): ICommand {
    override fun execute() = try {
        r.direction = r.direction + r.angularVelocity
    } catch (e: Exception) {
        println("   $e")
        throw Exception("Ошибка Вращения", e)
    }
}
