package ru.drvshare.main_patterns.starwars.cmds.rotate

interface IRotable {
    var direction: Direction
    val angularVelocity: Int
}
