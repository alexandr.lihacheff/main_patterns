package ru.drvshare.main_patterns.starwars.cmds.fuel

import ru.drvshare.main_patterns.common.ICommand
import ru.drvshare.main_patterns.starwars.queue.exceptions.CommandException

class CheckFuelCommand(private val fuel: IFuelable) : ICommand {
    override fun execute() {
        if (fuel.currentFuelAmount <= fuel.criticalFuelAmount) throw CommandException("Топливо всё")
    }
}
