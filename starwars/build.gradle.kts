plugins {
    kotlin("jvm")
//    application
    id("com.google.devtools.ksp")
}

dependencies {
    ksp(project(":codegen"))
    kspTest(project(":codegen"))
    implementation(project(":codegen"))

    implementation(project(":common"))
    implementation(project(":ioc"))
}

kotlin {
    sourceSets {
        val mockkVersion: String by project
        val kotlinVersion: String by project
        val kotlinLoggingJvmVersion: String by project
        val logbackVersion: String by project

        @Suppress("UNUSED_VARIABLE")
        val main by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
            }
        }
        @Suppress("UNUSED_VARIABLE")
            val test by getting {
                dependencies {
                    implementation(kotlin("test-junit"))
                    implementation("io.mockk:mockk:$mockkVersion")
                    implementation(project(":queue"))
                }
            }
            dependencies {
                // log
                implementation("ch.qos.logback:logback-classic:$logbackVersion")
                implementation("io.github.microutils:kotlin-logging-jvm:$kotlinLoggingJvmVersion")
                implementation(project(":ioc"))
                implementation(project(":queue"))
                implementation(project(":common"))
        }
    }
    jvmToolchain(11)
}
